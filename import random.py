import random
import math

t2 = [random.randint(0,500) for i in range(100)]
print(t2)

def normalize(mean, std, x):
    return (x-mean)/std

def project_on_normal_law(u, phi, x):
    return (1/(phi*math.sqrt(2*math.pi))*math.e**((-1/2)*((x-u)/phi)**2))
                                                  
def get_std(array, mean):
    somup = 0
    for elt in array:
        somup += math.sqrt((elt-mean)**2)
    return somup
                                                  
def get_normal_parameters_from_tab(array):
    som = 0
    nb_of_elements = len(array)
    for element in array:
        som += element
    mean = som/nb_of_elements
    std = (1/nb_of_elements)*get_std(array, mean)
    return mean, std

mean, std = get_normal_parameters_from_tab(t2)
print("moyenne : ",mean, "std : ", std)

def compare_2_arrays_of_100_int(t1, t2):
    i = 0
    array_length = len(t1)
    if len(t1) != len(t2):
        return -1
    distance = 0
    while(i < array_length):
        distance += math.sqrt((t2[i]-t1[i])**2)
    return distance

t1 = [random.randint(0,500) for j in range(100)]
print("t1")
result_comparaison = compare_2_arrays_of_100_int(t1,t2)
print("resultat de comparaison : ", result_comparaison)

def project_on_normal_law(u, phi, x):
	return (1/(phi*math.sqrt(2*math.pi))*math.e**((-1/2)*((w-u)/phi)**2))

def get_mean(array):
	length = len(array)
	i = 0
	somme = 0.0
	while(i<length):
		somme += array[i]
		i+=1
	moyenne = (somme//length)
	return moyenne

def get_std(array, mean):
	somup = 0
	length = float(len(array))
	for elt in array:
		somup += math.sqrt((elt-mean)**2)
	return somup//length

# to be continued...
